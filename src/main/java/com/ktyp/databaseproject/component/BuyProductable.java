/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.ktyp.databaseproject.component;

import com.ktyp.databaseproject.model.Product;

/**
 *
 * @author toey
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}

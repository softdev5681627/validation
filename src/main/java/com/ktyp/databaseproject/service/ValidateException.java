/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ktyp.databaseproject.service;

/**
 *
 * @author toey
 */
public class ValidateException extends Exception{

    public ValidateException(String message) {
        super(message);
    }
    
    
}
